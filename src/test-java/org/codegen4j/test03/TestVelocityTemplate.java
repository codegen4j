package org.codegen4j.test03;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.codegen4j.template.velocity.VelocityTemplate;

public class TestVelocityTemplate extends TestCase {

	public void test01() throws IOException {
		
		VelocityTemplate template = new VelocityTemplate(new File("src/data/test03/template.vm"));
		template.addHelper("h1", "  this is the helper ");
		template.addParam("p1", "this is the param");
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("a", "value of a");
		data.put("b", "and b value");
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		template.merge("input-name", data, "ouput-name", output);
		
		byte[] bytes = output.toByteArray();
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		BufferedReader reader = new BufferedReader(new InputStreamReader(in)); 
		String line = reader.readLine();
		assertEquals("line 1", "hello", line);
		line = reader.readLine();
		assertEquals("line 2", "value of a", line);
		line = reader.readLine();
		assertEquals("line 3", "and b value", line);
		line = reader.readLine();
		assertEquals("line 4", "this is the param", line);
		line = reader.readLine();
		assertEquals("line 5", "this is the helper", line);
		line = reader.readLine();
		assertEquals("line 6", "bye", line);
		
	}
}
