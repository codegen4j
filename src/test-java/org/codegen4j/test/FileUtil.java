package org.codegen4j.test;

import java.io.File;

public class FileUtil {

	static public void delete(File x) {
		if ( x.isFile() && x.exists() ) {
			x.delete();
			
		} else if ( x.isDirectory() && x.exists() ) {
			File[] children = x.listFiles();
			for(int i = 0; i < children.length; i++) {
				delete(children[i]);
			}
			x.delete();
		}
	}
}
