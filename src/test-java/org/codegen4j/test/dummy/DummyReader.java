package org.codegen4j.test.dummy;

import java.io.File;

import org.codegen4j.CodeReader;

public class DummyReader implements CodeReader {

	public Object readInstance(File source) {
		return source.getAbsolutePath();
	}

	public long timestamp() {
		return 0;
	}

}
