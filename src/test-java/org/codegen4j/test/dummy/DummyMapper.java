package org.codegen4j.test.dummy;

import org.codegen4j.Mapper;

public class DummyMapper implements Mapper {

	public String map(String source) {
		return source + ".dummy";
	}

}
