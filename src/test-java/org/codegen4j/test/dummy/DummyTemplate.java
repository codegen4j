package org.codegen4j.test.dummy;

import java.io.OutputStream;
import java.util.List;

import org.codegen4j.Template;

public class DummyTemplate implements Template {

	public long timestamp() {
		return 0;
	}

	public void merge(String sourceName, Object sourceModel, String outputName,
			OutputStream output) {
	}

	public void merge(List<Object> sourceModels, String outputName,
			OutputStream output) {
	}
}
