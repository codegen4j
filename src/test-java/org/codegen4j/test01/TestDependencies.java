package org.codegen4j.test01;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.codegen4j.CodeGenerator;
import org.codegen4j.GenerateReport;
import org.codegen4j.OutputType;
import org.codegen4j.exception.CannotReadSourceFileException;
import org.codegen4j.test.FileUtil;
import org.codegen4j.test.dummy.DummyMapper;
import org.codegen4j.test.dummy.DummyReader;
import org.codegen4j.test.dummy.DummyTemplate;

public class TestDependencies extends TestCase {

	public void testCannotReadSourceFile() {
		
		File output = new File("target/test01/output");

		FileUtil.delete(output);
		
		CodeGenerator generator = new CodeGenerator();
		
		generator.setSourcesBaseDir(new File("src/data/test01/source"));
		List<String> sources = new ArrayList<String>();
		sources.add("org/codegen4j/test/file0.txt");
		generator.setSources(sources);
		
		generator.setOutput(output, OutputType.DIR, false);

		generator.setMapper(new DummyMapper());
		
		generator.setReader(new DummyReader());
		
		generator.setTemplate(new DummyTemplate());

		generator.setDetailedReport(true);

		try {
			GenerateReport report = generator.generate();
			fail("should have thrown a CannotReadSourceFileException");
		} catch (CannotReadSourceFileException x) {
			assertEquals("file", sources.get(0), x.getFile());
		}
				
	}

	public void testGenerate() {
		
		File output = new File("target/test01/output");

		FileUtil.delete(output);
		
		CodeGenerator generator = new CodeGenerator();
		
		generator.setSourcesBaseDir(new File("src/data/test01"));
		List<String> sources = new ArrayList<String>();
		sources.add("org/codegen4j/test/file1.txt");
		sources.add("org/codegen4j/test/file2.txt");
		generator.setSources(sources);
		
		generator.setOutput(output, OutputType.DIR, false);

		generator.setMapper(new DummyMapper());
		
		generator.setReader(new DummyReader());
		
		generator.setTemplate(new DummyTemplate());

		generator.setDetailedReport(true);
		
		GenerateReport report = generator.generate();

		assertEquals("report.count", 2, report.getCount());
		
		assertTrue("file1.txt.dummy", new File(output, "org/codegen4j/test/file1.txt.dummy").exists());
		assertTrue("file2.txt.dummy", new File(output, "org/codegen4j/test/file2.txt.dummy").exists());
		
		assertEquals("report file1", "org/codegen4j/test/file1.txt.dummy", report.getFiles().get(0));
		assertEquals("report file2", "org/codegen4j/test/file2.txt.dummy", report.getFiles().get(1));
				
	}

}
