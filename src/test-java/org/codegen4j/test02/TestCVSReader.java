package org.codegen4j.test02;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import junit.framework.TestCase;

import org.codegen4j.reader.csv.CSVReader;

public class TestCVSReader extends TestCase {

	public void test01() {
		String ls = System.getProperty("line.separator");
		String cvs = "Abc" + ls +
		             "" + ls +
		             "def, gh, i  " + ls +
		             "  jkl, mno" + ls +
		             ls + ls;
		
		Reader input = new StringReader(cvs);
		          
		CSVReader reader = new CSVReader();
		List<List<String>> rows = reader.readInstance(input, "mypath");
		
		assertEquals("rows.size", 3, rows.size());
		assertEquals("rows[0].size", 1, rows.get(0).size());
		assertEquals("rows[1].size", 3, rows.get(1).size());
		assertEquals("rows[2].size", 2, rows.get(2).size());
		assertEquals("rows[0][0]", "Abc", rows.get(0).get(0));
		assertEquals("rows[1][0]", "def", rows.get(1).get(0));
		assertEquals("rows[1][1]", "gh", rows.get(1).get(1));
		assertEquals("rows[1][2]", "i", rows.get(1).get(2));
		assertEquals("rows[2][0]", "jkl", rows.get(2).get(0));
		assertEquals("rows[2][1]", "mno", rows.get(2).get(1));
		
		
	}
}
