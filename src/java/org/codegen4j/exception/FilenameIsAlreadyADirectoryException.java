package org.codegen4j.exception;

public class FilenameIsAlreadyADirectoryException extends RuntimeException {

	public FilenameIsAlreadyADirectoryException(String file) {
		super("output file is already a directory: " + file);
	}
}
