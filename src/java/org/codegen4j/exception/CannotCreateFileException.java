package org.codegen4j.exception;

public class CannotCreateFileException extends RuntimeException {

	public CannotCreateFileException(String msg) {
		super(msg);
	}
}
