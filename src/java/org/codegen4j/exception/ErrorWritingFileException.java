package org.codegen4j.exception;

public class ErrorWritingFileException extends RuntimeException {

	public ErrorWritingFileException(String file, Exception x) {
		super("error writing file: " + file, x);
	}
}
