package org.codegen4j.exception;

public class CannotWriteFileException extends RuntimeException {

	public CannotWriteFileException(String file, Exception x) {
		super("cannot write file: " + file, x);
	}
}
