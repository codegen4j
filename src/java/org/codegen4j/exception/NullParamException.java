package org.codegen4j.exception;

public class NullParamException extends RuntimeException {

	public NullParamException(String msg) {
		super(msg);
	}
}
