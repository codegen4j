package org.codegen4j.exception;

public class CannotCreateDirException extends RuntimeException {

	public CannotCreateDirException(String msg) {
		super(msg);
	}
}
