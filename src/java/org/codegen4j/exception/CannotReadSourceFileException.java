package org.codegen4j.exception;

public class CannotReadSourceFileException extends RuntimeException {

	String file;
	
	public CannotReadSourceFileException(String file) {
		super("cannot read source file: " + file);
		this.file = file;
	}
	
	public String getFile() {
		return file;
	}
}
