package org.codegen4j;

public interface Mapper {

	/**
	 * maps a source filename to a output filename.  
	 * @param source filename (can be a relative path)
	 * @return nulls if source filename does not comply with mapping roules.
	 */
	String map(String source);
}
