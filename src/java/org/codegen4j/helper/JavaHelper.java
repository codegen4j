package org.codegen4j.helper;

public class JavaHelper {

	
    /**
     * obtiene el nombre simple de la clase java correspondiente a un nombre de archivo.<br/>
     * Examples: <br/>
     *  - "org/codegen4j/test/Foo.java" -> "Foo"<br/>
     *  - "org/codegen4j/test/FooVO.vo" -> "FooVO"<br/>
     *  - "org\codegen4j\test\Foo.java" -> "Foo"<br/>
     * @param p_filename nombre del archivo:
     * @return String
         */
    public String simpleName(String filename) {
    	
    	filename = filename.replace('\\', '/');

        int index = filename.lastIndexOf("/");
        String file = null;
        if ( index < 0 ) {
            file = filename;
        } else {
            file = filename.substring(index + 1);
        }
        int end = file.indexOf(".");
        if ( end < 0 ) {
            return file;
        }
        return file.substring(0, end);
    }

	/**
	 * obtiene el nombre del package correspondiente a un nombre de archivo.<br/>
	 * Examples: <br/>
	 *  - "org/codegen4j/test/Foo.java" -> "org.codegen4j.test"<br/>
	 *  - "org/codegen4j/test/FooVO.vo" -> "org.codegen4j.test"<br/>
	 *  - "org/codegen4j\test\Foo.java" -> "org.codegen4j.test"<br/>
	 * @param file with the relative path to the file
	 * @return String package name.
	 */
	public String classPackage(String file) {

		file = file.replace('\\', '/');

		int index = file.lastIndexOf("/");
		if ( index < 0 ) {
			return "";
		}
		return file.substring(0, index).replace('/', '.');

	}

	/**
	 * obtains the parent package.<br/>
	 * Ejemplo:<br/>
	 *  - "org.codegen4j.test" -> "org.codegen4j"<br/>
	 * @param pkg java package
	 * @return String parent java package
	 */
	public String parentPackage(String pkg) {
		int index = pkg.lastIndexOf(".");
		if ( index > 0 ) {
			return pkg.substring(0, index);
		}
		return pkg;
	}
	
    /**
     * Makes the first letter capital.</br>
     * Example:</br>
     *  - "name" -> "Name"
     * @param string String
     * @return String
     */
    public String firstToUpperCase(String string) {
      if ( string == null ) {
          return "";
      }
      String post = string.substring(1,string.length());
      String first = (""+string.charAt(0)).toUpperCase();
      return first+post;
    }
}
