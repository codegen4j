package org.codegen4j.template.velocity;

import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.codegen4j.Template;
import org.codegen4j.template.velocity.exception.CouldNotLoadVelocityTemplateException;
import org.codegen4j.template.velocity.exception.CouldNotMergeVelocityTemplateException;
import org.codegen4j.template.velocity.exception.VelocityTemplateErrorException;
import org.codegen4j.util.FileUtil;

/**
 * Implements templating using Velocity language.
 * @author Luis Longeri
 *
 */
public class VelocityTemplate implements Template {

	Map<String, Object> helperMap = new TreeMap<String, Object>();
	Map<String, Object> paramMap = new TreeMap<String, Object>();
	File template;

	public VelocityTemplate(File template) {
		this.template = template;
	}

	public void addHelper(String name, Object helper) {
		helperMap.put(name, helper);
	}

	public void addParam(String name, Object param) {
		paramMap.put(name, param);
	}

	public void merge(String sourceName, Object sourceModel, String outputName, OutputStream output) {

		VelocityContext context = new VelocityContext();
		context.put("template", FileUtil.toUnixSlash(template.getAbsolutePath()));
		Map<String, String> source = new HashMap<String, String>();
		source.put("file", sourceName);
		File file = new File(sourceName);
		source.put("name", file.getName());
		source.put("path", file.getParent());
		context.put("source", source);
		context.put("destination", outputName);
		context.put("model", sourceModel);
		context.put("helpers", helperMap);
		context.put("params", paramMap);

		doMerge(loadTemplate(template), context, output);
	}

	public void merge(List<Object> sourceModels, String outputName, OutputStream output) {

		VelocityContext context = new VelocityContext();
		context.put("template", FileUtil.toUnixSlash(template.getAbsolutePath()));
		context.put("destination", outputName);
		context.put("models", sourceModels);
		context.put("helpers", helperMap);
		context.put("params", paramMap);

		doMerge(loadTemplate(template), context, output);

	}

	public long timestamp() {
		System.out.println("template last: " + template.lastModified());
		return template.lastModified();
	}

	private org.apache.velocity.Template loadTemplate(File file) {
		org.apache.velocity.Template template;
		try {
			VelocityEngine engine = new org.apache.velocity.app.VelocityEngine();
			String dir = file.getParent();
			engine.setProperty("file.resource.loader.path", dir);
			engine.init();
			template = engine.getTemplate(file.getName());
		}
		catch (ParseErrorException x) {
			throw new VelocityTemplateErrorException(file.getAbsolutePath(), x);
		}
		catch (Exception x) {
			throw new CouldNotLoadVelocityTemplateException(file.getAbsolutePath(), x);
		}
		return template;
	}

	private void doMerge(org.apache.velocity.Template template,
                         VelocityContext context, OutputStream output) {
		try {
			StringWriter buffer = new StringWriter();
			template.merge(context, buffer);
			Writer writer = new OutputStreamWriter(output);
			writer.write(buffer.toString());
			writer.flush();
		} catch (Exception x) {
			throw new CouldNotMergeVelocityTemplateException(x);
		}
	}

}
