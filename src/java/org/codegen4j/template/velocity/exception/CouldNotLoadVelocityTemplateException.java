package org.codegen4j.template.velocity.exception;

public class CouldNotLoadVelocityTemplateException extends RuntimeException {

	public CouldNotLoadVelocityTemplateException(String file, Exception x) {
		super("velocity template could not be loaded: " + file, x);
	}
}
