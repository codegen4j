package org.codegen4j.template.velocity.exception;

public class VelocityTemplateErrorException extends RuntimeException {

	public VelocityTemplateErrorException(String file, Exception x) {
		super("velocity template with parse error: " + file, x);
	}
}
