package org.codegen4j.template.velocity.exception;

public class CouldNotMergeVelocityTemplateException extends RuntimeException {
	
	public CouldNotMergeVelocityTemplateException(Exception x) {
		super(x);
	}

}
