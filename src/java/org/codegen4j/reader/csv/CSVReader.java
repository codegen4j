package org.codegen4j.reader.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import org.codegen4j.CodeReader;
import org.codegen4j.exception.CannotReadSourceFileException;

/**
 * Reader of CVS text files.
 * @author Luis Longeri
 */
public class CSVReader implements CodeReader {

	private char separator = ',';

	public void setSeparator(char s) {
		separator = s;
	}
	
	public List<List<String>> readInstance(File file) {
		String path = file.getAbsolutePath();
		try {
			return readInstance(new FileReader(file), path);
		} catch (FileNotFoundException e) {
			throw new CannotReadSourceFileException(path);
		}
	}
	
	/**
	 * Decodes a CVS file separated with the configured separator character.
	 * @param file: file to read.
	 * @return rows with items read.
	 */
	public List<List<String>> readInstance(Reader inputReader, String path) {

		List<List<String>> rows = new LinkedList<List<String>>(); 

		try {
			BufferedReader reader = new BufferedReader(inputReader);

			for(String line = reader.readLine(); line != null; line = reader.readLine()) {
				line = line.trim();
				if ( line.length() == 0) {
					continue;
				}
				List<String> row = new LinkedList<String>();
				int idx;
				while((idx = line.indexOf(separator)) >= 0) {
					String item = line.substring(0, idx).trim();
					row.add(item);
					line = line.substring(idx + 1);
				}
				row.add(line.trim());
				rows.add(row);
			}
		} catch ( IOException x) {
			throw new CannotReadSourceFileException(path);
		}
		return rows;
	}

	public long timestamp() {
		return 0;
	}

}
