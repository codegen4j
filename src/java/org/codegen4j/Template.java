package org.codegen4j;

import java.io.OutputStream;
import java.util.List;

public interface Template {

	/*
	 * Template's date (example: file date). 0 means no date.
	 */
	long timestamp();
	
	void merge(String sourceName, Object sourceModel, String outputName, OutputStream output);
	
	void merge(List<Object> sourceModels, String outputName, OutputStream output);
}
