package org.codegen4j;

import java.io.File;
import java.util.Map;

public interface CodeReader {

	/*
	 * CodeReader's date (example: some file date). 0 means no date.
	 */
	long timestamp();
	
	public Object readInstance(File source);

}
