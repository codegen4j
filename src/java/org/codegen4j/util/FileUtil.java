package org.codegen4j.util;

import java.io.File;

public class FileUtil {

    static public String toUnixSlash(String path) {
        if ( File.separatorChar == '/' ) {
            return path;
        }
        return path.replace(File.separatorChar, '/');
    }
}
