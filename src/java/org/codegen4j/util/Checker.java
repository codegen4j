package org.codegen4j.util;

import java.io.File;

import org.codegen4j.exception.NullParamException;

public class Checker {

	public static void notNull(Object param, String msg) {
		
		if ( param == null ) {
			throw new NullParamException(msg);
		}
		
	}

}
