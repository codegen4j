package org.codegen4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.codegen4j.exception.CannotCreateDirException;
import org.codegen4j.exception.CannotCreateFileException;
import org.codegen4j.exception.CannotReadSourceFileException;
import org.codegen4j.exception.CannotWriteFileException;
import org.codegen4j.exception.ErrorWritingFileException;
import org.codegen4j.exception.FilenameIsAlreadyADirectoryException;
import org.codegen4j.util.Checker;

/**
 * This class is used to generate code. It has to configured with the input files information, 
 * ouput destination, source code reader, template to use and optionaly helper clases and 
 * global parameters for the template execution.
 * @author Luis Longeri
 *
 */
public class CodeGenerator {

	private File sourcesBaseDir;
	private List<String> sources = new ArrayList<String>();
	
	private File output;
	private OutputType outputType;
	private boolean forceOverwrite;
	
	private Mapper mapper;

	private CodeReader reader;
	private Template template;
	
	private boolean detailedReport;
	
	public void setForceOverwrite(boolean forceOverwrite) {
		this.forceOverwrite = forceOverwrite;
	}

	public void setDetailedReport(boolean detailedReport) {
		this.detailedReport = detailedReport;
	}

	public void setSourcesBaseDir(File sourcesBaseDir) {
		this.sourcesBaseDir = sourcesBaseDir;
	}

	public void setSources(List<String> sources) {
		if ( sources != null ) {
			this.sources = sources;
		}
	}
	
	public void addSourceFile(String file) {
		sources.add(file);
	}
	
	public void setOutput(File output, OutputType type, boolean forceOverwrite) {
		this.output = output;
		this.outputType = type;
		this.forceOverwrite = forceOverwrite;
	}
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public void setReader(CodeReader reader) {
		this.reader = reader;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}
	
	public GenerateReport generate() {
		
		Checker.notNull(output, "output file/dir.");
		Checker.notNull(outputType, "output type.");
		Checker.notNull(template, "template.");
		Checker.notNull(reader, "code reader.");
		
		GenerateReport report = new GenerateReport();
		
		if ( outputType == OutputType.DIR ) {
			if ( output.isFile() ) {
				throw new CannotCreateDirException("output path is already a file.");
			}
			output.mkdirs();
		} else {
			if ( output.isDirectory() ) {
				throw new CannotCreateFileException("output path is already a directory");
			}
		}
		
		long baseTimestamp = template.timestamp();
		baseTimestamp = lastTimestamp(baseTimestamp, reader.timestamp());
		
		if ( outputType == OutputType.DIR ) {
			
			output.mkdirs();
			
			for(String source: sources) {
				String dest = mapper.map(source);
				if ( dest == null ) {
					continue;
				}
				File file = new File(sourcesBaseDir, source);
				if ( ! file.exists() || file.isDirectory() ) {
					throw new CannotReadSourceFileException(source);
				}
				
				long fileTimestamp = lastTimestamp(baseTimestamp, file.lastModified());
				
				Object model = reader.readInstance(file);
				
				OutputStream generated = openGenerated(output, dest, fileTimestamp);
				if ( generated == null ) {
					continue; // outfile is newer.
				}
				template.merge(source, model, dest, generated);

				close(dest, generated);
				
				report.incCount();
				if ( detailedReport ) {
					report.addFile(dest);
				}
			}
		} else {

			boolean overwrite = forceOverwrite; 
			if ( output.exists()) {
				if ( output.isDirectory()) {
					throw new FilenameIsAlreadyADirectoryException(output.getAbsolutePath());
				}
			} else {
				overwrite = true;
			}

			if ( !overwrite ) {
				long outputTimestamp = output.lastModified();
				if ( outputTimestamp < baseTimestamp) {
					overwrite = true;
				} else {
					for(String source: sources) {
						File file = new File(sourcesBaseDir, source);
						if ( ! file.exists() || file.isDirectory() ) {
							throw new CannotReadSourceFileException(source);
						}
						long fileTimestamp = lastTimestamp(baseTimestamp, file.lastModified());
						if ( outputTimestamp < file.lastModified()) {
							overwrite = true;
							break;
						}
					}
				}
			}
			
			if ( overwrite ) {
				List models = new ArrayList(sources.size());
				for(String source: sources) {
					File file = new File(sourcesBaseDir, source);
					if ( ! file.exists() || file.isDirectory() ) {
						throw new CannotReadSourceFileException(source);
					}
					Object model = reader.readInstance(file);
					models.add(model);
				}
				OutputStream generated = openGenerated(output, null, 0);
				if ( generated != null ) {
					template.merge(models, output.getAbsolutePath(), generated);
					close(output.getAbsolutePath(), generated);
					report.incCount();
					if ( detailedReport ) {
						report.addFile(output.getAbsolutePath());
					}
				}
			}
		}
		
		return report;
	}

	private void close(String file, OutputStream stream) {
		try {
			stream.close();
		} catch (IOException e) {
			throw new ErrorWritingFileException(file, e);
		}
	}

	private OutputStream openGenerated(File output2, String dest, long fileTimestamp) {
		OutputStream generated;
		File outFile = dest == null ? output : new File(output, dest);
		if ( outFile.exists()) {
			if ( outFile.isDirectory()) {
				throw new FilenameIsAlreadyADirectoryException(dest);
			}
			if ( fileTimestamp > 0 && !forceOverwrite && outFile.lastModified() >= fileTimestamp) {
				return null;
			}
		} else {
			outFile.getParentFile().mkdirs();
		}
		
		try {
			generated = new FileOutputStream(outFile);
		} catch (FileNotFoundException e) {
			throw new CannotWriteFileException(dest, e);
		}
		return generated;
	}

	/**
	 * return the latetest timestamp.
	 * @param t1
	 * @param t2
	 * @return
	 */
	private long lastTimestamp(long t1, long t2) {
		if ( t1 == 0 ) {
			return t2;
		}
		if ( t2 == 0 ) {
			return t1;
		}
		return t1 > t2 ? t1 : t2;
	}

}
