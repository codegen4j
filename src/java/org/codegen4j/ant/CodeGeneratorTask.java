package org.codegen4j.ant;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.util.SourceFileScanner;
import org.codegen4j.CodeGenerator;
import org.codegen4j.CodeReader;
import org.codegen4j.GenerateReport;
import org.codegen4j.Mapper;
import org.codegen4j.OutputType;
import org.codegen4j.reader.csv.CSVReader;

public class CodeGeneratorTask extends Task {

    private String type;
    private String classname;
    
    FileSet source = null;

    private List<AntTemplate> templates = new LinkedList<AntTemplate>();

	public void setType(String p_type) {
        type = p_type;
    }
	
	public void addFileset(FileSet fs) {
		//      System.out.println("adding fileset");
		if (source == null) {
			source = fs;
		}
		else {
			throw new BuildException(
					"only one fileset element must be defined.");
		}
	}
    
	public void addConfiguredTemplate(AntTemplate template) {
		template.validate();
		if (template.isEnabled()) {
			templates.add(template);
		}
	}
	
    public void execute() throws BuildException {

        long typeLastMod;
        log("starting code generation.", Project.MSG_VERBOSE);

        SourceFileScanner sfs = new SourceFileScanner(this);
        
        String[] files = scanSourceFileSet();

        File srcDir = source.getDir(getProject());
        Set<String> fileSet = new HashSet<String>(files.length);
        arrayToCollection(files, fileSet);
        
        System.out.println("fileSet: " + fileSet);
        
        CodeReader reader;
        
        if ( "csv".equals(type)) {
        	reader = new CSVReader();
        } else {
        	throw new BuildException("unknown type '" + type + "'.");
        }
        
        for (AntTemplate template: templates) {
        	File todir = template.getTodir();
        	String tofile = template.getTofile();
        	
        	log("template file: " + template.getFile().getAbsoluteFile(), Project.MSG_VERBOSE);
        	if ( todir != null ) {
        		log("template todir: " + todir.getAbsoluteFile(), Project.MSG_VERBOSE);
        		
                String[] makes = sfs.restrict(files, srcDir, todir,
                        template.getFileNameMapper());
        		List<String> sources = new ArrayList<String>(makes.length);
        		
        		fileSet.removeAll(sources);
        		
        		long lastmod = template.getTemplate().timestamp();
        		for(String file: fileSet) {
        			File f = new File( srcDir, file);
        			if ( f.lastModified() < lastmod ) {
        				sources.add(file);
        			}
        		}
        		
        		arrayToCollection(makes, sources);
        		
        		System.out.println("sources: " + sources);
        		
                CodeGenerator generator = new CodeGenerator();
                
                generator.setSourcesBaseDir(srcDir);
        		generator.setSources(sources);
        		
        		generator.setOutput(todir, OutputType.DIR, false);

        		generator.setMapper((Mapper) template.getFileNameMapper());
        		
        		generator.setReader(reader);
        		
        		generator.setTemplate(template.getTemplate());

        		generator.setDetailedReport(true);
        		
        		GenerateReport report = generator.generate();
        	}
        	else if ( tofile != null ) {
        		log("template tofile: " + tofile, Project.MSG_VERBOSE);
        		
                CodeGenerator generator = new CodeGenerator();
                
                generator.setSourcesBaseDir(srcDir);
        		List<String> sources = new ArrayList<String>(files.length);
        		arrayToCollection(files, sources);
        		generator.setSources(sources);
        		
        		generator.setOutput(new File(tofile), OutputType.FILE, false);

        		generator.setMapper((Mapper) template.getFileNameMapper());
        		
        		generator.setReader(reader);
        		
        		generator.setTemplate(template.getTemplate());

        		generator.setDetailedReport(true);
        		
        		GenerateReport report = generator.generate();
        	}
            
        }

    }
    
    private String[] scanSourceFileSet() throws BuildException {
        if ( source == null ) {
            throw new BuildException("a fileset element mush be defined for input source.");
        }

        DirectoryScanner ds = source.getDirectoryScanner(getProject());
        String[] files = ds.getIncludedFiles();
        log("file set size: " + files.length, Project.MSG_VERBOSE);
        return files;
    }

    private void arrayToCollection(String[] files, Collection<String> fileSet) {
        for (int j = 0; j < files.length; j++) {
            fileSet.add(files[j]);
        }
    }
}
