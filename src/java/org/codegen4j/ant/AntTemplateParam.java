package org.codegen4j.ant;

import java.io.File;

import org.apache.tools.ant.BuildException;

public class AntTemplateParam {

    private String name;
    private String value;
    private File propertyFile;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public File getPropertyFile() {
        return propertyFile;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setPropertyFile(File propertyFile) {
        this.propertyFile = propertyFile;
    }

	public void validate() {

		if ( getName() == null ) {
        	throw new BuildException("name of param not configured.");
        }
		if ( getValue() == null && getPropertyFile() == null ) {
        	throw new BuildException("value or property file of param '" + getName() + "' not configured.");
        }

		if ( getValue() != null && getPropertyFile() != null ) {
        	throw new BuildException("value and property file of param '" + getName() + "' cannot be configured simultaneously.");
        }
		
		if ( getPropertyFile() != null ) {
            if (!getPropertyFile().exists() ||
                !getPropertyFile().isFile()) {
                throw new BuildException("property file not found: " + getPropertyFile().getAbsolutePath());
            }
		}
	}
}