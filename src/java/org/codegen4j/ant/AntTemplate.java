package org.codegen4j.ant;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.util.FileNameMapper;
import org.codegen4j.Template;
import org.codegen4j.template.velocity.VelocityTemplate;

public class AntTemplate {

    private Project project;
    
    boolean force = false;
    boolean append = false;
    boolean enabled = true;
    
    private File file;
    private File todir = null;
    private String tofile = null;

    private FileNameMapper filenamemapper = null;
    boolean hasMapper = false;
    
    private Map<String, Object> paramMap = new TreeMap<String, Object>();
    private Map<String, AntHelper> helperMap = new TreeMap<String, AntHelper>();

    public AntTemplate(Project x) {
        project = x;
    }

	public void validate() {
        if ( getFile() == null ) {
            throw new BuildException("missing attribute 'file' in 'template' element. It must point to a template file.");
        }
        if ( getTodir() == null && getTofile() == null ) {
            throw new BuildException("attribute 'todir' xor 'tofile' mush be defined in 'template' element.");
        }
        if ( getTodir() != null && getTofile() != null ) {
            throw new BuildException("attributes 'todir' and 'tofile' cannot be defined together in 'template' element.");
        }
        if ( getTodir() != null && getFileNameMapper() == null ) {
            throw new BuildException("'pathmaper' element must be defined in this 'template' element.");
        }
	}

    public void setEnabled(boolean enabled) {
    	this.enabled = enabled;
    }
    
    public boolean isEnabled() {
    	return enabled;
    }
        
    public void setAppend(boolean append) {
		this.append = append;
	}

	public void setForce(boolean force) {
		this.force = force;
	}
    
	public boolean isAppend() {
		return append;
	}

	public boolean isForce() {
		return force;
	}

    public void setFile(File p_file) {
        file = p_file;
    }

    public File getFile() {
        return file;
    }

    public void setTodir(File p_todir) {
        todir = p_todir;
    }

    public File getTodir() {
        return todir;
    }

    public void setTofile(String p_tofile) {
        tofile = p_tofile;
    }

    public String getTofile() {
        return tofile;
    }
    
    public Map paramMap() {
        return paramMap;
    }

    public void addConfiguredHelper(AntHelper helper) {
    	helper.validate();
    	helperMap.put(helper.getName(), helper);
    }
    
    public void addConfiguredParam(AntTemplateParam param) throws BuildException {
    	
    	param.validate();
    	
        if ( param.getValue() != null ) {
            paramMap.put(param.getName(), param.getValue());
        } else if ( param.getPropertyFile() != null ) {

        	try {
	        	FileInputStream in = new FileInputStream(param.getPropertyFile());
	        	Properties props = new Properties();
	        	props.load(in);
	        	in.close();
	            paramMap.put(param.getName(), props);
        	} catch ( Exception x) {
        		throw new BuildException("error reading property file: " + param.getPropertyFile().getAbsolutePath(), x);
        	}
        }
    }
    
    public void addConfiguredPathmapper(AntPathFileNameMapper p_mapper)  throws BuildException {
        if (hasMapper) {
            throw new BuildException("Cannot define more than one mapper");
        }
        if ( p_mapper.getFrom() == null ) {
            throw new BuildException("missing 'from' attribute in javamapper");
        }
        if ( p_mapper.getTo() == null ) {
            throw new BuildException("missing 'to' attribute in javamapper");
        }
        hasMapper = true;
        filenamemapper = p_mapper;
    }

    public FileNameMapper getFileNameMapper() {
//        if ( mapperElement != null ) {
//            return mapperElement.getImplementation();
//        }
        if ( filenamemapper != null) {
            return filenamemapper;
        }
        throw new BuildException("missing 'mapper' or 'javamapper' element in template");
    }

	public Template getTemplate() {
		VelocityTemplate template = new VelocityTemplate(getFile());
		for(Map.Entry<String, Object> param: paramMap.entrySet()) {
			template.addParam(param.getKey(), param.getValue());
		}
		for(Map.Entry<String, AntHelper> helper: helperMap.entrySet()) {
			template.addHelper(helper.getKey(), helper.getValue().getInstance());
		}
		
		return template;
	}
}
