package org.codegen4j.ant;

import org.apache.tools.ant.BuildException;

public class AntHelper {

	private String name;
	private String classname;

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}
	
	public void validate() {
		
		if ( name == null ) {
			throw new BuildException("helper with out attribute 'name'");
		}
		if ( classname == null ) {
			throw new BuildException("classname of helper '" + name + "' missing");
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Object getInstance() {
		
		try {
			Class clazz = Class.forName(classname);
			return clazz.newInstance();
		} catch (Exception x) {
			throw new BuildException("cannot instanciate helper '" + name + "' of class '" + classname + "'");
		} catch (NoClassDefFoundError x ) {
			throw new BuildException("cannot instanciate helper '" + name + "' of class '" + classname + "'");
		}
	}
}
