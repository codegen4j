package org.codegen4j;

import java.util.ArrayList;
import java.util.List;

public class GenerateReport {

	private int count;
	private List<String> files = new ArrayList<String>();
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	public void incCount() {
		count++;
	}
	
	public List<String> getFiles() {
		return files;
	}
	public void setFiles(List<String> files) {
		this.files = files;
	}
	
	public void addFile(String file) {
		files.add(file);
	}
	
	
}
