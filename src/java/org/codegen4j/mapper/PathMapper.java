package org.codegen4j.mapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.codegen4j.Mapper;

public class PathMapper implements Mapper {

    private String from;
    private String to;

    protected List fromParts;
    private List toParts;

    public void setFrom(String p_from) {
        from = p_from;
        fromParts = split(from);
    }

    public void setTo(String p_to) {
        to = p_to;
        toParts = split(to);
    }

    public String map(String sourceFileName) {
        String map = map1(sourceFileName);
        if ( map != null ) {
            for (;;) {
                int idx = map.indexOf("/../");
                if ( idx <= 0 ) {
                    break;
                }
                int start = map.substring(0, idx-1).lastIndexOf("/");
                if ( start < 0 ) {
                    start = -1;
                }
                map = map.substring(0, start + 1) + map.substring(idx + 4);
            }
        }
        return map;
    }

    protected String map1(String sourceFileName) {
        sourceFileName = sourceFileName.replace('\\', '/');
        List wilds = new ArrayList();
        if ( match(fromParts, sourceFileName, 0, wilds) ) {
            StringBuffer buffer = new StringBuffer();
            Iterator wi = wilds.iterator();
            for (Iterator i = toParts.iterator(); i.hasNext(); ) {
                String part = (String) i.next();
                if ( part.startsWith("*") ) {
                    if ( wi.hasNext() ) {
                        buffer.append(wi.next());
                    } else {
                        buffer.append(part);
                    }
                } else {
                    buffer.append(part);
                }
            }
            return buffer.toString();
        }
        return null;
    }

    protected List split(String pattern) {
        java.util.StringTokenizer tok = new java.util.StringTokenizer(pattern, "*", true);
        List parts = new ArrayList();

        int wilds = 0;
        while (tok.hasMoreTokens()) {
            String token = tok.nextToken();
            if ("*".equals(token)) {
                wilds++;
            }
            else {
                if (wilds == 1) {
                    parts.add("*");
                }
                else if (wilds > 1) {
                    parts.add("**");
                }
                wilds = 0;
                parts.add(token);
            }
//                System.out.println("token:" + token);
        }
        if (wilds == 1) {
            parts.add("*");
        }
        else if (wilds > 1) {
            parts.add("**");
        }
        return parts;
    }

    protected boolean match(List p_parts, String p_target, int p_index, List wilds) {
        boolean ok = match2(p_parts, p_target, p_index, wilds);
        if ("**".equals(p_parts.get(p_parts.size() - 1))) {
            int idx = getFrom().lastIndexOf("**");
            wilds.remove(wilds.size() - 1);
            wilds.add(p_target.substring(idx));
        }
        return ok;
    }
    protected boolean match2(List p_parts, String p_target, int p_index, List wilds) {

        boolean match = true;
        int index = p_index;
        int len = p_target.length();

        int count = 0;
        for (Iterator i = p_parts.iterator(); i.hasNext(); ) {
            String part = (String) i.next();
            count++;
            boolean simpleWild = "*".equals(part);
            boolean doubleWild = "**".equals(part);

            if (simpleWild || doubleWild) {

                int a = index;
                int b = a;

                int end = len;
                if (simpleWild) {
                    end = p_target.indexOf("/", index);
                    if (end <= 0) {
                        end = len;
                        b = end;
                    }
                }

                List subWilds = new ArrayList();

                if (i.hasNext()) {
                    // hay algo mas.
                    String next = (String) i.next();
                    count++;
                    List subParts = p_parts.subList(count, p_parts.size());
                    int idx = index;
                    match = false;
                    for (idx = p_target.indexOf(next, idx); idx >= 0;
                         idx = p_target.indexOf(next, idx + 1)) {
                        int begin = idx + next.length();
                        if (idx > end) {
                            break;
                        }

                        if (match2(subParts, p_target.substring(begin, len), 0, subWilds)) {
                            match = true;
                            b = idx;
                            end = len;
                            wilds.add(p_target.substring(a, b));
                            wilds.addAll(subWilds);
                            break;
                        }
                        subWilds.clear();
                    }
                    if (match) {
                        index = end;
                        break;
                    }
                }
                else {
                    end = len;
                }

                wilds.add(p_target.substring(a, b));
                wilds.addAll(subWilds);
                index = end;

            }
            else {
                if (p_target.startsWith(part, index)) {
                    index += part.length();
                }
                else {
                    match = false;
                }
            }
            if (!match) {
                break;
            }
        }
        if (index != len) {
            match = false;
        }
        return match;
    }

    public String[] mapFileName(String sourceFileName) {
        String map = map(sourceFileName);
        if ( map != null ) {
            return new String[]{map};
        }
        return null;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

}
